const loadText = document.querySelector(".loading-text");
const backgroundImage = document.querySelector(".background-image");

let counter = 0;

let int = setInterval(blurring, 30);

function blurring() {
  counter++;
  if (counter > 99) {
    clearInterval(int);
  }
  loadText.innerText = `${counter}%`;
  loadText.style.opacity = scale(counter, 0, 100, 0, 1);
  backgroundImage.style.filter = `blur(${scale(counter, 0, 100, 30, 0)}px)`;
}

const scale = (num, in_min, in_max, out_min, out_max) => {
  return ((num - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
};
